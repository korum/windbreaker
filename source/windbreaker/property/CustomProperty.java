/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.property;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Represents a mutable property with a custom getter and setter.
 * @author Zane Jacobs
 * @param <T> The data type that this property stores.
 */
public class CustomProperty<T> {
	
	private final Supplier<T> getter;
	private final Consumer<T> setter;
	
	/**
	 * Creates a property with a custom getter and setter.
	 * @param getter A function which returns a value of type <code>T</code>.
	 * @param setter A function which accepts a value of type <code>T</code>.
	 */
	public CustomProperty(Supplier<T> getter, Consumer<T> setter) {
		this.getter = getter;
		this.setter = setter;
	}
	
	/**
	 * Creates a property with a custom getter and setter and initial value.
	 * @param getter A function which returns a value of type <code>T</code>.
	 * @param setter A function which accepts a value of type <code>T</code>.
	 * @param initialValue A value to pass to this property's setter upon creation.
	 */
	public CustomProperty(Supplier<T> getter, Consumer<T> setter, T initialValue) {
		this(getter, setter);
		set(initialValue);
	}
	
	/**
	 * @return The value of this property.
	 */
	public T get() {
		return getter.get();
	}
	
	/**
	 * @param value The new value of this property.
	 */
	public void set(T value) {
		setter.accept(value);
	}
}