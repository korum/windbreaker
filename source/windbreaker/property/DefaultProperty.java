/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.property;

/**
 * Represents a mutable property with a plain getter and setter.
 * @author Zane Jacobs
 * @param <T> The data type that this property stores.
 */
public class DefaultProperty<T> {
	
	private T value;
	
	/**
	 * Creates a property.
	 */
	public DefaultProperty() {}
	
	/**
	 * Creates a property with an initial value.
	 * @param initialValue This property's initial value.
	 */
	public DefaultProperty(T initialValue) {
		set(initialValue);
	}
	
	/**
	 * @return The value of this property.
	 */
	public T get() {
		return value;
	}
	
	/**
	 * @param value The new value of this property.
	 */
	public void set(T value) {
		this.value = value;
	}
}