/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.helper;

import java.util.Objects;

/**
 * Represents a mutable pair of objects.
 * @author Zane Jacobs
 * @param <A> The type of the first object.
 * @param <B> The type of the second object.
 */
public class Pair<A, B> {
	
	/**
	 * The first object.
	 */
	public A a;
	
	/**
	 * The second object.
	 */
	public B b;
	
	/**
	 * Default constructor. No initialization is performed.
	 */
	public Pair() {}
	
	/**
	 * Creates a pair of objects.
	 * @param a The fist object.
	 * @param b The second object.
	 */
	public Pair(A a, B b) {
		this.a = a;
		this.b = b;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Pair) {
			Pair other = (Pair)obj;
			return a.equals(other.a) && b.equals(other.b);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 83 * hash + Objects.hashCode(a);
		hash = 83 * hash + Objects.hashCode(b);
		return hash;
	}
}