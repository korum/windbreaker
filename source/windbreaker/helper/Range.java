/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.helper;

/**
 * Represents a range of values.
 * @author Zane Jacobs
 */
public class Range {
	
	/**
	 * One bound of the range.
	 */
	public double offset;
	
	/**
	 * The distance from offset to the other bound of the range.
	 */
	public double size;
	
	/**
	 * Default constructor. No initialization is performed.
	 */
	public Range() {}
	
	/**
	 * @param offset The offset of this range.
	 * @param size The size of this range.
	 */
	public Range(double offset, double size) {
		this.offset = offset;
		this.size = size;
	}
	
	/**
	 * @param offset An offset.
	 * @param size A size.
	 * @return A range in which offset will mark the lower bound of the range, and size will be
	 * non-negative.
	 */
	public static Range positiveSize(double offset, double size) {
		if(size < 0) {
			return new Range(offset + size, -size);
		} else {
			return new Range(offset, size);
		}
	}
}