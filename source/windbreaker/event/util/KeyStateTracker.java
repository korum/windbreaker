/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.util;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import windbreaker.event.keychange.KeyChangeEvent;
import windbreaker.event.keychange.KeyChangeType;
import windbreaker.event.keychange.KeyLocation;
import windbreaker.helper.Pair;

/**
 * Tracks the pressed status of certain keys either by key-code, or by both key-code and
 * key-location.
 * @author Zane Jacobs
 */
public class KeyStateTracker implements Consumer<KeyChangeEvent> {
	
	private final Map<Integer, Boolean> keyStates = new HashMap<>();
	private final Map<Pair<Integer, KeyLocation>, Boolean> locatedKeyStates = new HashMap<>();
	
	/**
	 * Adds a key to track by key-code.
	 * @param keyCode The key-code of the key to track.
	 */
	public void addKey(int keyCode) {
		keyStates.put(keyCode, false);
	}
	
	/**
	 * Adds a key to track by key-code and key-location.
	 * @param keyCode The key-code of the key to track.
	 * @param keyLocation The key-location of the key to track.
	 */
	public void addKey(int keyCode, KeyLocation keyLocation) {
		locatedKeyStates.put(new Pair<>(keyCode, keyLocation), false);
	}
	
	/**
	 * Removes a key tracked by key-code.
	 * @param keyCode The key-code of the key to stop tracking.
	 */
	public void removeKey(int keyCode) {
		keyStates.remove(keyCode);
	}
	
	/**
	 * Removes a key tracked by key-code and key-location.
	 * @param keyCode The key-code of the key to stop tracking.
	 * @param keyLocation The key-location of the key to stop tracking.
	 */
	public void removeKey(int keyCode, KeyLocation keyLocation) {
		locatedKeyStates.remove(new Pair<>(keyCode, keyLocation));
	}
	
	/**
	 * @param keyCode The key-code of the key to test.
	 * @return True if this KeyStateTracker tracks a key by key-code with the given key-code, false
	 * otherwise.
	 */
	public boolean isTrackingKey(int keyCode) {
		return keyStates.containsKey(keyCode);
	}
	
	/**
	 * @param keyCode The key-code of the key to test.
	 * @param keyLocation The key-locations of the key to test.
	 * @return True if this KeyStateTracker tracks a key by key-code and key-location which has the
	 * given key-code and key-location, false otherwise.
	 */
	public boolean isTrackingKey(int keyCode, KeyLocation keyLocation) {
		return locatedKeyStates.containsKey(new Pair<>(keyCode, keyLocation));
	}
	
	/**
	 * @param keyCode The key-code of a tracked key to test.
	 * @return True if a key being tracked by key-code with the given key-code is pressed. False if
	 * that key is up.
	 */
	public boolean isPressed(int keyCode) {
		return keyStates.get(keyCode);
	}
	
	/**
	 * @param keyCode The key-code of the key to test.
	 * @param keyLocation The key-location of the key to test.
	 * @return True if a key being tracked by key-code and key-location with the given key-code and
	 * key-location is pressed. False if that key is up.
	 */
	public boolean isPressed(int keyCode, KeyLocation keyLocation) {
		return locatedKeyStates.get(new Pair<>(keyCode, keyLocation));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void accept(KeyChangeEvent e) {
		boolean pressed = e.keyChange == KeyChangeType.PRESSED;
		updateKeyMap(keyStates, e.keyCode, pressed);
		updateKeyMap(locatedKeyStates, new Pair<>(e.keyCode, e.keyLocation), pressed);
	}
	
	private static <K> void updateKeyMap(Map<K, Boolean> keyMap, K key, boolean pressed) {
		if(keyMap.containsKey(key)) {
			keyMap.put(key, pressed);
		}
	}
	
	/**
	 * @param keyCodes Key-codes of the keys to track.
	 * @return A KeyStateTracker which tracks keys with the given key-codes.
	 */
	public static KeyStateTracker createTrackerForKeys(int... keyCodes) {
		KeyStateTracker keyStateTracker = new KeyStateTracker();
		for(int keyCode : keyCodes) {
			keyStateTracker.addKey(keyCode);
		}
		return keyStateTracker;
	}
}