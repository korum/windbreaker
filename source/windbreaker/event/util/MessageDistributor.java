/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Distributes a message to many listeners. The order in which listeners are notified is the order
 * in which they are added to the MessageDistributor.
 * @author Zane Jacobs
 * @param <T> The type of the message.
 */
public class MessageDistributor<T> implements Consumer<T> {
	
	private final List<Consumer<T>> listeners = new ArrayList<>();
	
	/**
	 * Adds a listener to this MessageDistributor. Listeners may be added more than once.
	 * @param listener A listener to add to this MessageDistributor.
	 */
	public void addListener(Consumer<T> listener) {
		listeners.add(listener);
	}
	
	/**
	 * Removes a listener from this MessageDistributor.
	 * @param listener A listener to remove from this MessageDistributor.
	 */
	public void removeListener(Consumer<T> listener) {
		listeners.remove(listener);
	}
	
	/**
	 * @param listener A listener.
	 * @return True if the given listener is in this MessageDistributor, false otherwise.
	 */
	public boolean containsListener(Consumer<T> listener) {
		return listeners.contains(listener);
	}
	
	/**
	 * @return The number of listeners in this MessageDistributor.
	 */
	public int getListenerCount() {
		return listeners.size();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void accept(T message) {
		for(Consumer<T> listener : listeners) {
			listener.accept(message);
		}
	}
}