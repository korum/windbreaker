/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.mousebutton;

import windbreaker.geom.Vector2;

/**
 * Encapsulates the information associated with a mouse button event. A mouse button event occurs
 * when the state of the mouse buttons changes.
 * @author Zane Jacobs
 */
public class MouseButtonEvent {
	
	/**
	 * The button whose state changed.
	 */
	public final MouseButtonType button;
	
	/**
	 * The location of the cursor within the window.
	 */
	public final Vector2 position;
	
	/**
	 * The type of change of this mouse button event. Has the value <code>PRESSED</code> when a
	 * button that was previously up was pressed. Has the value <code>RELEASED</code> when a key
	 * that was previously down was released.
	 */
	public final MouseButtonChange buttonChange;
	
	/**
	 * Creates a new <code>MouseButtonEvent</code> with the specified button, position, and change
	 * type.
	 * @param button The button of this <code>MouseButtonEvent</code>.
	 * @param position The position of this <code>MouseButtonEvent</code>.
	 * @param buttonChange The change type of this <code>MouseButtonEvent</code>.
	 */
	public MouseButtonEvent(MouseButtonType button, Vector2 position, MouseButtonChange buttonChange) {
		this.button = button;
		this.position = position;
		this.buttonChange = buttonChange;
	}
}