/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.mousebutton;

/**
 * Indicates the type of change of a mouse button event.
 * @author Zane Jacobs
 */
public enum MouseButtonChange {
	
	/**
	 * Indicates that a button which was previously up was pressed.
	 */
	PRESSED,
	
	/**
	 * Indicates that a button which was previously down was released.
	 */
	RELEASED;
}