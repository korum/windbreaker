/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.mousebutton;

/**
 * Indicates the button whose state changed in a mouse button change event.
 * @author Zane Jacobs
 */
public enum MouseButtonType {
	
	/**
	 * Indicates that the state of left mouse button changed.
	 */
	LEFT,
	
	/**
	 * Indicates that the state of right mouse button changed.
	 */
	RIGHT,
	
	/**
	 * Indicates that the state of middle mouse button changed.
	 */
	MIDDLE;
}