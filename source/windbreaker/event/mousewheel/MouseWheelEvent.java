/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.mousewheel;

import windbreaker.geom.Vector2;

/**
 * Encapsulates the information associated with a mouse wheel event. A mouse wheel event occurs when
 * the mouse wheel is turned.
 * @author Zane Jacobs
 */
public class MouseWheelEvent {
	
	/**
	 * The amount the mouse wheel has turned. Units are in wheel-ticks. Fractional values may occur
	 * for hardware which supports higher-resolution wheels.
	 */
	public final double turn;
	
	/**
	 * The position of the cursor within the window.
	 */
	public final Vector2 position;
	
	/**
	 * Creates a new <code>MouseWheelEvent</code> with the specified turn and position.
	 * @param turn The turn of this <code>MouseWheelEvent</code>.
	 * @param position The position of this <code>MouseWheelEvent</code>.
	 */
	public MouseWheelEvent(double turn, Vector2 position) {
		this.turn = turn;
		this.position = position;
	}
}