/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.mousemove;

import windbreaker.geom.Vector2;

/**
 * Encapsulates the information associated with a mouse move event. A mouse move event occurs when
 * cursor moves into, out of, or within the window.
 * @author Zane Jacobs
 */
public class MouseMoveEvent {
	
	/**
	 * The move type of this mouse move event. Has the value <code>ENTER</code> when the cursor
	 * enters the window, <code>EXIT</code> when the cursor exits the window, and <code>MOVE</code>
	 * when the cursor moves within the window.
	 */
	public final MouseMoveType moveType;
	
	/**
	 * The position of the cursor within the window.
	 */
	public final Vector2 position;
	
	/**
	 * Creates a new <code>MouseMoveEvent</code> with the specified move type and position.
	 * @param moveType The move type of this <code>MouseMoveEvent</code>.
	 * @param position The position of this <code>MouseMoveEvent</code>.
	 */
	public MouseMoveEvent(MouseMoveType moveType, Vector2 position) {
		this.moveType = moveType;
		this.position = position;
	}
}