/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.mousemove;

/**
 * Indicates the type of move of a mouse move event.
 * @author Zane Jacobs
 */
public enum MouseMoveType {
	
	/**
	 * Indicates that the cursor moved within the window.
	 */
	MOVE,
	
	/**
	 * Indicates that the cursor was previously outside of the window and has moved into the window.
	 */
	ENTER,
	
	/**
	 * Indicates that the cursor was previously inside the window and has moved outside of the
	 * window.
	 */
	EXIT;
}