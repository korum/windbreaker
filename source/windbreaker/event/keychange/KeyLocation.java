/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.keychange;

/**
 * Represents the location of a key.
 * @author Zane Jacobs
 */
public enum KeyLocation {
	
	/**
	 * Indicates a key which is located in the main keyboard.
	 */
	STANDARD,
	
	/**
	 * When there are two keys with the same key code on either side of the keyboard, this value
	 * indicates the key which is on the left.
	 */
	LEFT,
	
	/**
	 * When there are two keys with the same key code on either side of the keyboard, this value
	 * indicates the key which is on the right.
	 */
	RIGHT,
	
	/**
	 * When there is a key in the main keyboard and a key on the number pad which have the same key
	 * code, this value indicates the key which is on the number pad.
	 */
	NUMPAD;
}