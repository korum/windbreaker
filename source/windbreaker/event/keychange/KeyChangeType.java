/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.keychange;

/**
 * Indicates the type of change of a key change event.
 * @author Zane Jacobs
 */
public enum KeyChangeType {
	
	/**
	 * Indicates that a key which was previously up was pressed.
	 */
	PRESSED,
	
	/**
	 * Indicates that a key which was previously down was released.
	 */
	RELEASED;
}