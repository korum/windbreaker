/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.event.keychange;

/**
 * Encapsulates the information associated with a key change event. A key change event occurs when
 * the state of the keyboard changes.
 * @author Zane Jacobs
 */
public class KeyChangeEvent {
	
	/**
	 * A code which identifies the key whose state changed. Constants for every key code can be
	 * found in the <code>KeyEvent</code> class.
	 */
	public final int keyCode;
	
	/**
	 * Indicates the key whose state changed, when there are multiple keys with the same key code.
	 * For example, on a keyboard which has a left shift and a right shift, a key change for the
	 * left shift would have a <code>keyLocation</code> of <code>LEFT</code>, and a change in the
	 * right shift would have a <code>keyLocation</code> of <code>RIGHT</code>. A key change for a
	 * key located in the number pad will have a <code>keyLocation</code> of <code>NUMPAD</code>. 
	 * For all other key changes, the <code>keyLocation</code> will be <code>STANDARD</code>.
	 * have a keyLocation of 
	 */
	public final KeyLocation keyLocation;
	
	/**
	 * The type of change of this key change. Has the value <code>PRESSED</code> when a key that was
	 * previously up was pressed. Has the value <code>RELEASED</code> when a key that was previously
	 * down was released.
	 */
	public final KeyChangeType keyChange;
	
	/**
	 * Constructs a new <code>KeyChangeEvent</code> with the specified key code, location, and
	 * change type.
	 * @param keyCode The key code of this <code>KeyChangeEvent</code>.
	 * @param keyLocation The location of this <code>KeyChangeEvent</code>.
	 * @param keyChange The type of change of this <code>KeyChangeEvent</code>.
	 */
	public KeyChangeEvent(int keyCode, KeyLocation keyLocation, KeyChangeType keyChange) {
		this.keyCode = keyCode;
		this.keyLocation = keyLocation;
		this.keyChange = keyChange;
	}
}