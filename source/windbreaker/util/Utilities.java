/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.util;

import windbreaker.geom.Vector2;
import java.awt.Shape;
import java.util.function.Predicate;

/**
 * A container for generic utility functions.
 * @author Zane Jacobs
 */
public class Utilities {
	
	/**
	 * @param x A number to take the floor of.
	 * @return The greatest integer which is less than or equal to the argument.
	 */
	public static int floor(double x) {
		return (int) Math.floor(x);
	}
	
	/**
	 * @param x A number to take the ceil of.
	 * @return The least integer which is greater than or equal to the argument.
	 */
	public static int ceil(double x) {
		return (int) Math.ceil(x);
	}
	
	/**
	 * @param x A number to round toward zero.
	 * @return The integer with the greatest distance from zero whose distance from zero is less
	 * than or equal to the argument's distance from zero.
	 */
	public static int roundTowardZero(double x) {
		return (int) x;
	}
	
	/**
	 * @param shape A shape used to define a vector predicate.
	 * @return A predicate which will return true for any vector which lies inside of the specified
	 * shape.
	 */
	public static Predicate<Vector2> shapeToPredicate(Shape shape) {
		return v -> shape.contains(v.getX(), v.getY());
	}
	
	private Utilities() {}
}