/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.util;

import java.util.Iterator;

/**
 * A read-only view of an array.
 * @author Zane Jacobs
 * @param <T> The type of the array.
 */
public class ReadOnlyArray<T> implements Iterable<T> {
	
	private final T[] array;
	
	private ReadOnlyArray(T[] array) {
		this.array = array;
	}
	
	/**
	 * @return The number of elements in the array.
	 */
	public int size() {
		return array.length;
	}
	
	/**
	 * @param index The index of the element to get.
	 * @return The element at the given index.
	 */
	public T get(int index) {
		return array[index];
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<T> iterator() {
		return new ArrayIterator<>(array);
	}
	
	/**
	 * @param <T> The type of the array.
	 * @param array An array to wrap.
	 * @return A ReadOnlyArray which wraps the given array.
	 */
	public static <T> ReadOnlyArray<T> wrapArray(T[] array) {
		return new ReadOnlyArray<>(array);
	}
}