/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.geom;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import windbreaker.Transform;
import windbreaker.util.Utilities;

/**
 * Represents an immutable polygon. This class implements the Shape interface and can be used for
 * drawing.
 * @author Zane Jacobs
 */
public class Polygon2D implements Shape {
	
	private final List<Vector2> points;
	private final Rectangle2D bounds2D;
	private final Rectangle bounds;
	
	private Polygon2D(Iterable<Vector2> pointIterable) {
		List<Vector2> pointList = new ArrayList<>();
		for(Vector2 point : pointIterable) {
			pointList.add(point);
		}
		points = Collections.unmodifiableList(pointList);
		bounds2D = getBounds2D(points);
		bounds = getBounds(bounds2D);
	}
	
	/**
	 * @return An immutable list of the points in this polygon.
	 */
	public List<Vector2> getPoints() {
		return points;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Rectangle getBounds() {
		return bounds;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Rectangle2D getBounds2D() {
		return bounds2D;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(double x, double y) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Point2D point) {
		return contains(point.getX(), point.getY());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean intersects(double x, double y, double w, double h) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean intersects(Rectangle2D rectangle) {
		return intersects(
			rectangle.getX(),
			rectangle.getY(),
			rectangle.getWidth(),
			rectangle.getHeight()
		);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(double x, double y, double w, double h) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Rectangle2D rectangle) {
		return contains(
			rectangle.getX(),
			rectangle.getY(),
			rectangle.getWidth(),
			rectangle.getHeight()
		);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PathIterator getPathIterator(AffineTransform at) {
		if(at == null) {
			return PolygonPathIterator.createFromPolygon(this);
		} else {
			double[] matrix = new double[6];
			at.getMatrix(matrix);
			Transform transform = Transform.createFromMatrix(matrix);
			return PolygonPathIterator.createFromPolygonTransform(this, transform);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PathIterator getPathIterator(AffineTransform at, double flatness) {
		return new FlatteningPathIterator(getPathIterator(at), flatness);
	}
	
	private static Rectangle2D getBounds2D(Iterable<Vector2> points) {
		double minX = Double.MAX_VALUE;
		double minY = Double.MAX_VALUE;
		double maxX = -Double.MAX_VALUE;
		double maxY = -Double.MAX_VALUE;
		
		for(Vector2 point: points) {
			double x = point.getX();
			double y = point.getY();
			
			if(x < minX) {
				minX = x;
			}
			if(x > maxX) {
				maxX = x;
			}
			if(y < minY) {
				minY = y;
			}
			if(y > maxY) {
				maxY = y;
			}
		}
		
		double width = maxX - minX;
		double height = maxY - minY;
		return new Rectangle2D.Double(minX, minY, width, height);
	}
	
	private static Rectangle getBounds(Rectangle2D bounds2D) {
		return new Rectangle(
			Utilities.floor(bounds2D.getX()),
			Utilities.floor(bounds2D.getY()),
			Utilities.ceil(bounds2D.getWidth()),
			Utilities.ceil(bounds2D.getHeight())
		);
	}
	
	/**
	 * @param points The points which the polygon will contain.
	 * @return A polygon which contains the given points.
	 */
	public static Polygon2D createFromPoints(Iterable<Vector2> points) {
		return new Polygon2D(points);
	}
}