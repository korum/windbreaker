/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.geom;

/**
 * Represents an immutable point in a 2-dimensional space.
 * @author Noah Brown
 */
public class Vector2 {
	
	/**
	 * The zero vector.
	 */
	public static final Vector2 ZERO = Vector2.createFromXY(0, 0);
	
	/**
	 * The vector (1, 0).
	 */
	public static final Vector2 X_UNIT = Vector2.createFromXY(1, 0);
	
	/**
	 * The vector (0, 1).
	 */
	public static final Vector2 Y_UNIT = Vector2.createFromXY(0, 1);
	
	/**
	 * The vector (1, 1).
	 */
	public static final Vector2 UNIT_SUM = Vector2.add(X_UNIT, Y_UNIT);
	
	private final double x;
	private final double y;
	
	private Vector2(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @return The location of this vector on the x-axis.
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * @return The location of this vector on the y-axis.
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * @return The magnitude of this vector.
	 */
	public double getMagnitude() {
		return Math.sqrt(getMagnitudeSquared());
	}
	
	/**
	 * @return The magnitude of this vector raised to the power of 2.
	 */
	public double getMagnitudeSquared() {
		return dot(this, this);
	}
	
	/**
	 * @return The angle between this vector and the x-axis in radians, going positive clockwise.
	 */
	public double getAngle() {
		//TODO - Add angle logic.
		return 0;
	}
	
	/**
	 * @param factor The scaling factor to apply to this vector.
	 * @return This vector, scaled by the given factor.
	 */
	public Vector2 scale(double factor) {
		return createFromXY(
			factor * getX(),
			factor * getY()
		);
	}
	
	public Vector2 addX(double x) {
		return createFromXY(this.x + x, y);
	}
	
	public Vector2 addY(double y) {
		return createFromXY(x, this.y + y);
	}
	
	public Vector2 scaleX(double factor) {
		return createFromXY(factor * x, y);
	}
	
	public Vector2 scaleY(double factor) {
		return createFromXY(x, factor * y);
	}
	
	/**
	 * @return A string representation of this vector in ordered-pair notation.
	 */
	@Override
	public String toString() {
		return "(" + getX() + ", " + getY() + ")";
	}
	
	/**
	 * @param a The first operand.
	 * @param b The second operand.
	 * @return The sum of two vectors.
	 */
	public static Vector2 add(Vector2 a, Vector2 b) {
		return createFromXY(
			a.getX() + b.getX(),
			a.getY() + b.getY()
		);
	}
	
	/**
	 * @param a The minuend.
	 * @param b The subtrahend.
	 * @return The difference between two vectors.
	 */
	public static Vector2 subtract(Vector2 a, Vector2 b) {
		return createFromXY(
			a.getX() - b.getX(),
			a.getY() - b.getY()
		);
	}
	
	/**
	 * @param a The first operand.
	 * @param b The second operand.
	 * @return The dot product of two vectors.
	 */
	public static double dot(Vector2 a, Vector2 b) {
		return (a.getX() * b.getX()) + (a.getY() * b.getY());
	}
	
	/**
	 * @param a The first operand.
	 * @param b The second operand.
	 * @return A vector whose x and y components are the products of the x and y components of the
	 * arguments.
	 */
	public static Vector2 memberwiseMultiply(Vector2 a, Vector2 b) {
		return createFromXY(a.getX() * b.getX(), a.getY() * b.getY());
	}
	
	/**
	 * @param x The x component of the vector.
	 * @param y The y component of the vector.
	 * @return A vector which has the given x and y components.
	 */
	public static Vector2 createFromXY(double x, double y) {
		return new Vector2(x, y);
	}
	
	/**
	 * @param angle The angle of the vector.
	 * @param magnitude The magnitude of the vector.
	 * @return A vector which has the given angle and magnitude.
	 */
	public static Vector2 createFromAngleMagnitude(double angle, double magnitude) {
		return createFromXY(
			Math.cos(angle) * magnitude,
			Math.sin(angle) * magnitude
		);
	}
}