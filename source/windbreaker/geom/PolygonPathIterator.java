/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.geom;

import windbreaker.geom.Polygon2D;
import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.List;
import windbreaker.Transform;
import windbreaker.geom.Vector2;

public class PolygonPathIterator implements PathIterator {
	
	private final List<Vector2> points;
	private int index;
	
	private PolygonPathIterator(List<Vector2> points) {
		this.points = points;
	}
	
	@Override
	public int getWindingRule() {
		return PathIterator.WIND_EVEN_ODD;
	}
	
	@Override
	public boolean isDone() {
		return index > points.size();
	}
	
	@Override
	public void next() {
		index++;
	}
	
	private int getSegment() {
		if(index == 0) {
			return PathIterator.SEG_MOVETO;
		} else if(index == points.size()) {
			return PathIterator.SEG_CLOSE;
		} else {
			return PathIterator.SEG_LINETO;
		}
	}
	
	@Override
	public int currentSegment(float[] coords) {
		int segment = getSegment();
		if(segment != PathIterator.SEG_CLOSE) {
			assign(coords, points.get(index));
		}
		return segment;
	}
	
	@Override
	public int currentSegment(double[] coords) {
		int segment = getSegment();
		if(segment != PathIterator.SEG_CLOSE) {
			assign(coords, points.get(index));
		}
		return segment;
	}
	
	private static void assign(float[] coords, Vector2 point) {
		coords[0] = (float) point.getX();
		coords[1] = (float) point.getY();
	}
	
	private static void assign(double[] coords, Vector2 point) {
		coords[0] = point.getX();
		coords[1] = point.getY();
	}
	
	public static PolygonPathIterator createFromPolygon(Polygon2D polygon) {
		return new PolygonPathIterator(polygon.getPoints());
	}
	
	public static PolygonPathIterator createFromPolygonTransform(Polygon2D polygon, Transform transform) {
		List<Vector2> points = polygon.getPoints();
		List<Vector2> transformedPoints = new ArrayList<>(points.size());
		for(int k = 0; k < points.size(); k++) {
			transformedPoints.add(transform.apply(points.get(k)));
		}
		return new PolygonPathIterator(transformedPoints);
	}
}