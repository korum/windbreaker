/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.geom;

/**
 * Represents an immutable point in a 3-dimensional space.
 * @author Zane Jacobs
 */
public class Vector3 {
	
	/**
	 * The zero vector.
	 */
	public static final Vector3 ZERO = Vector3.createFromXYZ(0, 0, 0);
	
	/**
	 * The vector (1, 1, 1).
	 */
	public static final Vector3 UNIT_SUM = Vector3.createFromXYZ(1, 1, 1);
	
	private final double x;
	private final double y;
	private final double z;
	
	private Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * @return The location of this vector on the x-axis.
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * @return The location of this vector on the y-axis.
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * @return The location of this vector on the z-axis.
	 */
	public double getZ() {
		return z;
	}
	
	/**
	 * @return The magnitude of this vector.
	 */
	public double getMagnitude() {
		return Math.sqrt(getMagnitudeSquared());
	}
	
	/**
	 * @return The magnitude of this vector raised to the power of 2.
	 */
	public double getMagnitudeSquared() {
		return dot(this, this);
	}
	
	/**
	 * @param factor The scaling factor to apply to this vector.
	 * @return This vector, scaled by the given factor.
	 */
	public Vector3 scale(double factor) {
		return Vector3.createFromXYZ(
			factor * getX(),
			factor * getY(),
			factor * getZ()
		);
	}
	
	/**
	 * @return A string representation of this vector in ordered-triple notation.
	 */
	@Override
	public String toString() {
		return "(" + getX() + ", " + getY() + ", " + getZ() + ")";
	}
	
	/**
	 * @param a The first operand.
	 * @param b The second operand.
	 * @return The sum of two vectors.
	 */
	public static Vector3 add(Vector3 a, Vector3 b) {
		return Vector3.createFromXYZ(
			a.getX() + b.getX(),
			a.getY() + b.getY(),
			a.getZ() + b.getZ()
		);
	}
	
	/**
	 * @param a The minuend.
	 * @param b The subtrahend.
	 * @return The difference between two vectors.
	 */
	public static Vector3 subtract(Vector3 a, Vector3 b) {
		return Vector3.createFromXYZ(
			a.getX() - b.getX(),
			a.getY() - b.getY(),
			a.getZ() - b.getZ()
		);
	}
	
	/**
	 * @param a The first operand.
	 * @param b The second operand.
	 * @return The dot product of two vectors.
	 */
	public static double dot(Vector3 a, Vector3 b) {
		return (a.getX() * b.getX()) + (a.getY() * b.getY()) + (a.getZ() * b.getZ());
	}
	
	/**
	 * @param x The x component of the vector.
	 * @param y The y component of the vector.
	 * @param z The z component of the vector.
	 * @return A vector which has the given x, y, and z components.
	 */
	public static Vector3 createFromXYZ(double x, double y, double z) {
		return new Vector3(x, y, z);
	}
}