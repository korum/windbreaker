/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker;

import windbreaker.geom.Vector2;

/**
 * Represents an immutable affine transformation which may be combined with other transforms and
 * applied to vectors.
 * @author Zane Jacobs
 */
public class Transform {
	
	/**
	 * A Transform that does nothing.
	 */
	public static final Transform IDENTITY = new Transform(
		Vector2.X_UNIT,
		Vector2.Y_UNIT,
		Vector2.ZERO
	);
	
	private final Vector2 xBasis;
	private final Vector2 yBasis;
	private final Vector2 offset;
	
	private Transform(Vector2 xBasis, Vector2 yBasis, Vector2 offset) {
		this.xBasis = xBasis;
		this.yBasis = yBasis;
		this.offset = offset;
	}
	
	/**
	 * @return The x basis vector of this transform.
	 */
	public Vector2 getXBasis() {
		return xBasis;
	}
	
	/**
	 * @return The y basis vector of this transform.
	 */
	public Vector2 getYBasis() {
		return yBasis;
	}
	
	/**
	 * @return The offset of this transform.
	 */
	public Vector2 getOffset() {
		return offset;
	}
	
	/**
	 * @return The scale of this transform.
	 */
	public Vector2 getScale() {
		return Vector2.createFromXY(xBasis.getX(), yBasis.getY());
	}
	
	/**
	 * @return The shear of this transform.
	 */
	public Vector2 getShear() {
		return Vector2.createFromXY(yBasis.getX(), xBasis.getY());
	}
	
	/**
	 * @return A flat 2x3 matrix in column-major order which represents this transform.
	 */
	public double[] toMatrix() {
		return new double[]{
			xBasis.getX(), xBasis.getY(),
			yBasis.getX(), yBasis.getY(),
			offset.getX(), offset.getY()
		};
	}
	
	/**
	 * @param vector A vector to transform.
	 * @return The given vector transformed by this transform.
	 */
	public Vector2 apply(Vector2 vector) {
		return Vector2.add(
			Vector2.add(
				xBasis.scale(vector.getX()),
				yBasis.scale(vector.getY())
			),
			offset
		);
	}
	
	/**
	 * @return A string representation of this transform in matrix notation.
	 */
	@Override
	public String toString() {
		return
			"[" + xBasis.getX() + " " + yBasis.getX() + " " + offset.getX() + "]" +
			"[" + xBasis.getY() + " " + yBasis.getY() + " " + offset.getY() + "]";
	}
	
	/**
	 * @param a The multiplier.
	 * @param b The multiplicand.
	 * @return The combination of two transforms.
	 */
	public static Transform combine(Transform a, Transform b) {
		return Transform.createFromBasisOffset(
			Vector2.add(
				a.xBasis.scale(b.xBasis.getX()),
				a.yBasis.scale(b.xBasis.getY())
			),
			Vector2.add(
				a.xBasis.scale(b.yBasis.getX()),
				a.yBasis.scale(b.yBasis.getY())
			),
			Vector2.add(
				Vector2.add(
					a.xBasis.scale(b.offset.getX()),
					a.yBasis.scale(b.offset.getY())
				),
				a.offset
			)
		);
	}
	
	/**
	 * @param xBasis The x basis vector of the transform.
	 * @param yBasis The y basis vector of the transform.
	 * @param offset The offset of the transform.
	 * @return A transform which has the given basis vectors and offset.
	 */
	public static Transform createFromBasisOffset(Vector2 xBasis, Vector2 yBasis, Vector2 offset) {
		return new Transform(xBasis, yBasis, offset);
	}
	
	/**
	 * @param scale The scale of the transform.
	 * @param shear The shear of the transform.
	 * @param offset The offset of the transform.
	 * @return A transform which has the specified scale, shear, and offset.
	 */
	public static Transform createFromScaleShearOffset(Vector2 scale, Vector2 shear, Vector2 offset) {
		Vector2 xBasis = Vector2.createFromXY(scale.getX(), shear.getY());
		Vector2 yBasis = Vector2.createFromXY(shear.getX(), scale.getY());
		return Transform.createFromBasisOffset(xBasis, yBasis, offset);
	}
	
	/**
	 * @param matrix A flat 2x3 matrix in column-major order.
	 * @return A transform derived from the given matrix.
	 */
	public static Transform createFromMatrix(double[] matrix) {
		return new Transform(
			Vector2.createFromXY(matrix[4], matrix[5]),
			Vector2.createFromXY(matrix[0], matrix[3]),
			Vector2.createFromXY(matrix[2], matrix[1])
		);
	}
	
	/**
	 * @param translate A vector which defines the translation.
	 * @return A transform that will perform a translation.
	 */
	public static Transform createTranslation(Vector2 translate) {
		return Transform.createFromBasisOffset(Vector2.X_UNIT, Vector2.Y_UNIT, translate);
	}
	
	/**
	 * @param scale A vector which defines the scale.
	 * @return A transform that will perform a scale.
	 */
	public static Transform createScale(Vector2 scale) {
		return Transform.createFromScaleShearOffset(scale, Vector2.ZERO, Vector2.ZERO);
	}
	
	/**
	 * @param shear A vector which defines the shear.
	 * @return A transform that will perform a shear.
	 */
	public static Transform createShear(Vector2 shear) {
		return Transform.createFromScaleShearOffset(Vector2.UNIT_SUM, shear, Vector2.ZERO);
	}
	
	/**
	 * @param angle The angle of rotation in radians.
	 * @return A transform that will perform a rotation.
	 */
	public static Transform createRotation(double angle) {
		double cosAngle = Math.cos(angle);
		double sinAngle = Math.sin(angle);
		Vector2 xBasis = Vector2.createFromXY(cosAngle, sinAngle);
		Vector2 yBasis = Vector2.createFromXY(-sinAngle, cosAngle);
		return Transform.createFromBasisOffset(xBasis, yBasis, Vector2.ZERO);
	}
}