/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.gui;

import windbreaker.geom.Vector2;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import windbreaker.util.Utilities;

/**
 * Encapsulates a fixed, rectangular space of mutable pixel data.
 * @author Zane Jacobs
 */
public class Surface {
	
	private final BufferedImage image;
	private final Draw draw;
	
	private Surface(BufferedImage image) {
		this.image = image;
		draw = Draw.wrapGraphics(image.createGraphics());
	}
	
	/**
	 * @return The width of this surface in pixels.
	 */
	public int getWidth() {
		return image.getWidth();
	}
	
	/**
	 * @return The height of this surface in pixels.
	 */
	public int getHeight() {
		return image.getHeight();
	}
	
	/**
	 * @return The size of this surface as a vector.
	 */
	public Vector2 getSize() {
		return Vector2.createFromXY(getWidth(), getHeight());
	}
	
	/**
	 * @return An instance of Draw which may be used to draw onto this surface.
	 */
	public Draw getDraw() {
		return draw;
	}
	
	/**
	 * @param x The x-coordinate of the pixel to read.
	 * @param y The y-coordinate of the pixel to read.
	 * @return The color of the specified pixel.
	 */
	public Color getPixel(int x, int y) {
		return new Color(image.getRGB(x, y), true);
	}
	
	/**
	 * Sets the color of a pixel on this surface.
	 * @param x The x-coordinate of the pixel to write.
	 * @param y The y-coordinate of the pixel to write.
	 * @param color The color to write to the specified pixel.
	 */
	public void setPixel(int x, int y, Color color) {
		image.setRGB(x, y, color.getRGB());
	}
	
	/**
	 * @return This surface's underlying BufferedImage.
	 */
	public BufferedImage getImage() {
		return image;
	}
	
	/**
	 * @param width The width of the surface in pixels.
	 * @param height The height of the surface in pixels.
	 * @return A new, completely transparent surface with the specified width and height.
	 */
	public static Surface createNew(int width, int height) {
		return new Surface(new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB));
	}
	
	/**
	 * @param size The size of the surface in pixels. The floor of the x and y values are used.
	 * @return A new, completely transparent surface with the specified size.
	 */
	public static Surface createNew(Vector2 size) {
		return createNew(Utilities.floor(size.getX()), Utilities.floor(size.getY()));
	}
	
	/**
	 * @param filePath A path to an image file.
	 * @return A surface which contains the image from the specified file or null if there was an
	 * I/O error.
	 */
	public static Surface loadFile(String filePath) {
		try {
			return new Surface(ImageIO.read(new File(filePath)));
		} catch(IOException e) {
			return null;
		}
	}
}