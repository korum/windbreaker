/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.gui;

import windbreaker.util.Utilities;
import windbreaker.geom.Vector2;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import windbreaker.event.keychange.KeyChangeType;
import windbreaker.event.keychange.KeyChangeEvent;
import windbreaker.event.keychange.KeyLocation;
import windbreaker.event.keytype.KeyTypeEvent;
import windbreaker.event.mousebutton.MouseButtonType;
import windbreaker.event.mousebutton.MouseButtonChange;
import windbreaker.event.mousebutton.MouseButtonEvent;
import windbreaker.event.mousemove.MouseMoveEvent;
import windbreaker.event.mousemove.MouseMoveType;
import windbreaker.event.mousewheel.MouseWheelEvent;
import windbreaker.property.CustomProperty;
import windbreaker.property.DefaultProperty;

/**
 * Encapsulates a graphical window and game loop.
 * @author Zane Jacobs
 */
public class GameWindow {
	
	private static final long DEFAULT_DELAY = 10000000;
	private static final Map<Integer, KeyLocation> KEY_LOCATION_CODE_TO_ENUM;
	private static final Map<Integer, MouseButtonType> MOUSE_BUTTON_CODE_TO_ENUM;
	
	static {
		Map<Integer, KeyLocation> keyLocationCodeToEnum = new HashMap<>();
		keyLocationCodeToEnum.put(KeyEvent.KEY_LOCATION_STANDARD, KeyLocation.STANDARD);
		keyLocationCodeToEnum.put(KeyEvent.KEY_LOCATION_LEFT, KeyLocation.LEFT);
		keyLocationCodeToEnum.put(KeyEvent.KEY_LOCATION_RIGHT, KeyLocation.RIGHT);
		keyLocationCodeToEnum.put(KeyEvent.KEY_LOCATION_NUMPAD, KeyLocation.NUMPAD);
		KEY_LOCATION_CODE_TO_ENUM = Collections.unmodifiableMap(keyLocationCodeToEnum);
		
		Map<Integer, MouseButtonType> mouseButtonCodeToEnum = new HashMap<>();
		mouseButtonCodeToEnum.put(MouseEvent.BUTTON1, MouseButtonType.LEFT);
		mouseButtonCodeToEnum.put(MouseEvent.BUTTON2, MouseButtonType.RIGHT);
		mouseButtonCodeToEnum.put(MouseEvent.BUTTON3, MouseButtonType.MIDDLE);
		MOUSE_BUTTON_CODE_TO_ENUM = Collections.unmodifiableMap(mouseButtonCodeToEnum);
	}
	
	private final JFrame frame = new JFrame();
	private final GamePanel panel = new GamePanel();
	private volatile boolean runGameLoop = false;
	
	/**
	 * The delay between frames in nanoseconds.
	 */
	public final DefaultProperty<Long> delay = new DefaultProperty<>(DEFAULT_DELAY);
	
	/**
	 * The function called before drawing a frame.
	 */
	public final DefaultProperty<Runnable> updateFunc = new DefaultProperty<>(
		()->{}
	);
	
	/**
	 * The function which draws the frame.
	 */
	public final DefaultProperty<Consumer<Draw>> drawFunc = new DefaultProperty<>(
		draw -> {}
	);
	
	/**
	 * The handler for key change events. Fires when the state of the keyboard changes.
	 */
	public final DefaultProperty<Consumer<KeyChangeEvent>> keyChangeHandler = new DefaultProperty<>(
		e -> {}
	);
	
	/**
	 * The handler for key type events. Fires when a printable character has been typed.
	 */
	public final DefaultProperty<Consumer<KeyTypeEvent>> keyTypeHandler = new DefaultProperty<>(
		e -> {}
	);
	
	/**
	 * The handler for mouse button events. Fires when the state of a mouse button changes.
	 */
	public final DefaultProperty<Consumer<MouseButtonEvent>> mouseButtonHandler = new DefaultProperty<>(
		e -> {}
	);
	
	/**
	 * The handler for mouse move events. Fires when the cursor enters, exits, or moves within this
	 * window.
	 */
	public final DefaultProperty<Consumer<MouseMoveEvent>> mouseMoveHandler = new DefaultProperty<>(
		e -> {}
	);
	
	/**
	 * The handler for mouse wheel events. Fires when the mouse wheel moves.
	 */
	public final DefaultProperty<Consumer<MouseWheelEvent>> mouseWheelHandler = new DefaultProperty<>(
		e -> {}
	);
	
	/**
	 * The title of this window.
	 */
	public final CustomProperty<String> title = new CustomProperty<>(
		() -> frame.getTitle(),
		value -> frame.setTitle(value)
	);
	
	/**
	 * The position of this window relative to the screen. Units are in pixels. The floor of the
	 * values given will be the values used.
	 */
	public final CustomProperty<Vector2> position = new CustomProperty<>(
		() -> Vector2.createFromXY(frame.getX(), frame.getY()),
		value -> frame.setLocation(
			Utilities.floor(value.getX()),
			Utilities.floor(value.getY())
		)
	);
	
	/**
	 * The width and height of this window, in pixels. The floor of the values given will be be the
	 * values used.
	 */
	public final CustomProperty<Vector2> size = new CustomProperty<>(
		() -> Vector2.createFromXY(frame.getWidth(), frame.getHeight()),
		value -> frame.setSize(
			Utilities.floor(value.getX()),
			Utilities.floor(value.getY())
		)
	);
	
	/**
	 * Whether or not this window is resizable by the user.
	 */
	public final CustomProperty<Boolean> isResizable = new CustomProperty<>(
		() -> frame.isResizable(),
		value -> frame.setResizable(value)
	);
	
	/**
	 * Whether or not this window is visible.
	 */
	public final CustomProperty<Boolean> isVisible = new CustomProperty<>(
		() -> frame.isVisible(),
		value -> {
			frame.setVisible(value);
			frame.requestFocusInWindow();
			panel.requestFocusInWindow();
		}
	);
	
	/**
	 * The background color of this window.
	 */
	public final CustomProperty<Color> backgroundColor = new CustomProperty<>(
		() -> panel.getBackground(),
		value -> panel.setBackground(value)
	);
	
	/**
	 * Whether or not the game loop is running. When it is true, the update and draw functions will
	 * be called at regular intervals, as specified by delay.
	 */
	public final CustomProperty<Boolean> isRunningGameLoop = new CustomProperty<>(
		() -> runGameLoop,
		value -> {
			if(value != runGameLoop) {
				runGameLoop = value;
				if(value) {
					startGameLoop();
				}
			}
		}
	);
	
	/**
	 * Creates a new <code>GameWindow</code> with the specified title, position, size, and
	 * resizability.
	 * @param title The title of this <code>GameWindow</code>.
	 * @param position The location of this <code>GameWindow</code> on-screen.
	 * @param size The size of this <code>GameWindow</code>.
	 * @param resizable Indicates whether or not this <code>GameWindow</code> is resizable by the
	 * user.
	 * @param backgroundColor The background color of this <code>GameWindow</code>.
	 */
	public GameWindow(String title, Vector2 position, Vector2 size, boolean resizable,
		Color backgroundColor) {
		initialize(title, size, resizable, backgroundColor);
		frame.setLocation(
			Utilities.floor(position.getX()),
			Utilities.floor(position.getY())
		);
	}
	
	/**
	 * Creates a new <code>GameWindow</code> with the specified title, size, and resizability,
	 * centered within the screen.
	 * @param title The title of this <code>GameWindow</code>.
	 * @param size The size of this <code>GameWindow</code>.
	 * @param resizable Indicates whether or not this <code>GameWindow</code> is resizable by the
	 * user.
	 * @param backgroundColor The background color of this <code>GameWindow</code>.
	 */
	public GameWindow(String title, Vector2 size, boolean resizable, Color backgroundColor) {
		initialize(title, size, resizable, backgroundColor);
		frame.setLocationRelativeTo(null);
	}
	
	private void initialize(String title, Vector2 size, boolean resizable, Color backgroundColor) {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle(title);
		frame.setResizable(resizable);
		panel.setPreferredSize(new Dimension(
			Utilities.floor(size.getX()),
			Utilities.floor(size.getY())
		));
		panel.setBackground(backgroundColor);
		panel.addKeyListener(new GameKeyListener());
		panel.addMouseListener(new GameMouseListener());
		panel.addMouseMotionListener(new GameMouseMotionListener());
		panel.addMouseWheelListener(new GameMouseWheelListener());
		frame.add(panel);
		frame.pack();
	}
	
	/**
	 * Redraws the frame.
	 */
	public void redraw() {
		panel.repaint();
	}
	
	private void startGameLoop() {
		new Thread(() -> {
			while(runGameLoop) {
				long startTime = System.nanoTime();
				updateFunc.get().run();
				panel.repaint();
				while(System.nanoTime() - startTime < delay.get());
			}
		}).start();
	}
	
	private class GamePanel extends JPanel {
		
		@Override
		public void paint(Graphics graphics) {
			super.paint(graphics);
			drawFunc.get().accept(Draw.wrapGraphics((Graphics2D)graphics));
		}
	}
	
	private class GameKeyListener implements KeyListener {
		
		@Override
		public void keyTyped(KeyEvent e) {
			keyTypeHandler.get().accept(new KeyTypeEvent(e.getKeyChar()));
		}
		
		@Override
		public void keyPressed(KeyEvent e) {
			keyChangeHandler.get().accept(new KeyChangeEvent(
				e.getKeyCode(),
				KEY_LOCATION_CODE_TO_ENUM.get(e.getKeyLocation()),
				KeyChangeType.PRESSED
			));
		}
		
		@Override
		public void keyReleased(KeyEvent e) {
			keyChangeHandler.get().accept(new KeyChangeEvent(
				e.getKeyCode(),
				KEY_LOCATION_CODE_TO_ENUM.get(e.getKeyLocation()),
				KeyChangeType.RELEASED
			));
		}
	}
	
	private class GameMouseListener implements MouseListener {
		
		@Override
		public void mouseClicked(MouseEvent e) {}
		
		@Override
		public void mousePressed(MouseEvent e) {
			mouseButtonHandler.get().accept(new MouseButtonEvent(
				MOUSE_BUTTON_CODE_TO_ENUM.get(e.getButton()),
				Vector2.createFromXY(e.getX(), e.getY()),
				MouseButtonChange.PRESSED
			));
		}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			mouseButtonHandler.get().accept(new MouseButtonEvent(
				MOUSE_BUTTON_CODE_TO_ENUM.get(e.getButton()),
				Vector2.createFromXY(e.getX(), e.getY()),
				MouseButtonChange.RELEASED
			));
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			mouseMoveHandler.get().accept(new MouseMoveEvent(
				MouseMoveType.ENTER,
				Vector2.createFromXY(e.getX(), e.getY())
			));
		}
		
		@Override
		public void mouseExited(MouseEvent e) {
			mouseMoveHandler.get().accept(new MouseMoveEvent(
				MouseMoveType.EXIT,
				Vector2.createFromXY(e.getX(), e.getY())
			));
		}
	}
	
	private class GameMouseMotionListener implements MouseMotionListener {
		
		@Override
		public void mouseDragged(MouseEvent e) {
			mouseMoveHandler.get().accept(new MouseMoveEvent(
				MouseMoveType.MOVE,
				Vector2.createFromXY(e.getX(), e.getY())
			));
		}
		
		@Override
		public void mouseMoved(MouseEvent e) {
			mouseMoveHandler.get().accept(new MouseMoveEvent(
				MouseMoveType.MOVE,
				Vector2.createFromXY(e.getX(), e.getY())
			));
		}
	}
	
	private class GameMouseWheelListener implements MouseWheelListener {
		
		@Override
		public void mouseWheelMoved(java.awt.event.MouseWheelEvent e) {
			mouseWheelHandler.get().accept(new MouseWheelEvent(
				e.getPreciseWheelRotation(),
				Vector2.createFromXY(e.getX(), e.getY())
			));
		}
	}
}