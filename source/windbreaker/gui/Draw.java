/* WindBreaker
 * A Korum project
 * 
 */
package windbreaker.gui;

import windbreaker.util.Utilities;
import windbreaker.geom.Vector2;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import windbreaker.Transform;
import windbreaker.geom.Polygon2D;
import windbreaker.helper.Range;
import windbreaker.property.CustomProperty;
import windbreaker.property.DefaultProperty;

/**
 * Encapsulates a draw state and provides functions for drawing.
 * @author Zane Jacobs
 */
public class Draw {
	
	/**
	 * A color that is completely transparent. Drawing with this color will have no effect.
	 */
	public static final Color TRANSPARENT = new Color(0, true);
	
	private Graphics2D graphics;
	private Transform trans = Transform.IDENTITY;
	
	private Draw(Graphics2D graphics) {
		this.graphics = graphics;
	}
	
	//Draw state properties
	
	/**
	 * The color used to fill shapes.
	 */
	public final DefaultProperty<Color> fillColor = new DefaultProperty<>(Color.LIGHT_GRAY);
	
	/**
	 * The color used to outline shapes.
	 */
	public final DefaultProperty<Color> edgeColor = new DefaultProperty<>(Color.DARK_GRAY);
	
	/**
	 * The clipping boundary. Anything which would be drawn outside of the clipping area does not
	 * get drawn. null may be returned and may be used to reset the clipping area.
	 */
	public final CustomProperty<Shape> clip = new CustomProperty<>(
		() -> graphics.getClip(),
		value -> graphics.setClip(value)
	);
	
	/**
	 * The transform of this Draw. All drawing with this instance is transformed by this transform.
	 */
	public final CustomProperty<Transform> transform = new CustomProperty<>(
		() -> trans,
		value -> {
			trans = value;
			graphics.setTransform(new AffineTransform(trans.toMatrix()));
		}
	);
	
	//Draw functions
	
	/**
	 * Draws a single point using the edge color.
	 * @param point The point to draw.
	 */
	public void point(Vector2 point) {
		Rectangle2D.Double rectangle = new Rectangle2D.Double(point.getX(), point.getY(), 1, 1);
		graphics.setColor(edgeColor.get());
		graphics.fill(rectangle); 
	}
	
	/**
	 * Draws a line between two points using the edge color.
	 * @param a An endpoint the line to be drawn.
	 * @param b The other endpoint the line to be drawn.
	 */
	public void line(Vector2 a, Vector2 b) {
		Line2D.Double line = new Line2D.Double(a.getX(), a.getY(), b.getX(), b.getY());
		shape(line);
	}
	
	/**
	 * Draws a rectangle, filling it with the fill color and outlining it with the edge color.
	 * @param corner A corner of the rectangle to be drawn.
	 * @param size The size of the rectangle to be drawn.
	 */
	public void rectangle(Vector2 corner, Vector2 size) {
		Rectangle2D.Double rectangle = getRectangle(corner, size);
		shape(rectangle);
	}
	
	/**
	 * Draws a rectangle, filling it with the fill color and outlining it with the edge color.
	 * @param corner0 A corner of the rectangle to be drawn.
	 * @param corner1 The opposite corner of the rectangle to be drawn.
	 */
	public void rectangle2(Vector2 corner0, Vector2 corner1) {
		Vector2 size = Vector2.subtract(corner1, corner0);
		rectangle(corner0, size);
	}
	
	/**
	 * Draws an ellipse inscribed in a rectangle, filling it with the fill color and outlining it
	 * with the edge color.
	 * @param corner A corner of the bounding rectangle.
	 * @param size The size of the bounding rectangle.
	 */
	public void ellipse(Vector2 corner, Vector2 size) {
		Ellipse2D.Double ellipse = getEllipse(corner, size);
		shape(ellipse);
	}
	
	/**
	 * Draws an ellipse inscribed in a rectangle, filling it with the fill color and outlining it
	 * with the edge color.
	 * @param corner0 A corner of the bounding rectangle.
	 * @param corner1 The opposite corner of the bounding rectangle.
	 */
	public void ellipse2(Vector2 corner0, Vector2 corner1) {
		Vector2 size = Vector2.subtract(corner1, corner0);
		ellipse(corner0, size);
	}
	
	/**
	 * Draws a circle with a specified radius, centered at a specified point, filling it with the
	 * fill color and outlining it with the edge color.
	 * @param center The center of the circle.
	 * @param radius The radius of the circle.
	 */
	public void circle(Vector2 center, double radius) {
		Vector2 toCorner = Vector2.createFromXY(radius, radius);
		Vector2 corner = Vector2.subtract(center, toCorner);
		Vector2 size = toCorner.scale(2);
		ellipse(corner, size);
	}
	
	/**
	 * Draws a polygon, filling it with the fill color, and outlining it with the edge color.
	 * @param points
	 */
	public void polygon(Iterable<Vector2> points) {
		shape(Polygon2D.createFromPoints(points));
	}
	
	/**
	 * Draws an arbitrary shape, filling it with the fill color and outlining it with the edge
	 * color.
	 * @param shape The shape to draw.
	 */
	public void shape(Shape shape) {
		graphics.setColor(fillColor.get());
		graphics.fill(shape);
		graphics.setColor(edgeColor.get());
		graphics.draw(shape);
	}
	
	/**
	 * Draws a surface at a location.
	 * @param surface The surface to draw.
	 * @param position The position of the top-left corner of the surface.
	 */
 	public void surface(Surface surface, Vector2 position) {
		int x = Utilities.floor(position.getX());
		int y = Utilities.floor(position.getY());
		graphics.drawImage(surface.getImage(), x, y, null);
	}
	
	/**
	 * Draws a surface in a bounding rectangle.
	 * @param surface The surface to draw.
	 * @param corner A corner of the bounding rectangle.
	 * @param size The size of the bounding rectangle.
	 */
	public void surface(Surface surface, Vector2 corner, Vector2 size) {
		int x = Utilities.floor(corner.getX());
		int y = Utilities.floor(corner.getY());
		int width = Utilities.roundTowardZero(size.getX());
		int height = Utilities.roundTowardZero(size.getY());
		graphics.drawImage(surface.getImage(), x, y, width, height, null);
	}
	
	/**
	 * Draws a surface in a bounding rectangle, the size of which is the size of the image, scaled
	 * by the given scaling factor.
	 * @param surface The surface to draw.
	 * @param position A corner of the bounding rectangle.
	 * @param scale The scaling factor for the image size.
	 */
 	public void surfaceScaled(Surface surface, Vector2 position, Vector2 scale) {
		Vector2 size = Vector2.memberwiseMultiply(scale, surface.getSize());
		surface(surface, position, size);
	}
	
	/**
	 * Draws text at a location.
	 * @param text Some text to draw.
	 * @param position The upper-left corner of the text.
	 */
	public void text(String text, Vector2 position) {
		graphics.setColor(edgeColor.get());
		graphics.drawString(
			text,
			(float) position.getX(),
			(float) position.getY()
		);
	}
	
	/**
	 * Draws a surface in a bounding rectangle, the size of which is the size of the image, scaled
	 * by the given scaling factor.
	 * @param surface The surface to draw.
	 * @param position A corner of the bounding rectangle.
	 * @param scale The scaling factor for the image size.
	 */
	public void surfaceScaled(Surface surface, Vector2 position, double scale) {
		surfaceScaled(surface, position, Vector2.createFromXY(scale, scale));
	}
	
	private static Rectangle2D.Double getRectangle(Vector2 corner, Vector2 size) {
		Range x = Range.positiveSize(corner.getX(), size.getX());
		Range y = Range.positiveSize(corner.getY(), size.getY());
		return new Rectangle2D.Double(x.offset, y.offset, x.size, y.size);
	}
	
	private static Ellipse2D.Double getEllipse(Vector2 corner, Vector2 size) {
		Rectangle2D.Double rectangle = getRectangle(corner, size);
		return new Ellipse2D.Double(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
	}
	
	/**
	 * @param graphics A Graphics2D that this Draw will manipulate.
	 * @return An instance of Draw which wraps the specified Graphics2D.
	 */
	public static Draw wrapGraphics(Graphics2D graphics) {
		return new Draw(graphics);
	}
}