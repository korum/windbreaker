# Windbreaker
#### Lightweight Java Game Engine

Authors: Noah Brown ([nsbrown3@gmail.com](mailto:nsbrown3@gmail.com)) and Zane Jacobs ([zane.jacobs.0@gmail.com](mailto:zane.jacobs.0@gmail.com))

---

## Features
* Vector math
	* 2D and 3D vector classes with appropriate operations
* Surfaces - 2D spaces which contain pixel information
	* Can be manipulated in various ways (stretched, rotated, placed into other surfaces)
* GUI
	* Window management
	* Visual component organization
		* Placing of visual components
	* Input flow control
		* Handles where input goes, which component gets input
* Logic flow control
	* Game loop
	* Registering/unregistering of updatables

> Subject to change as insight is gained through development

---

## Contributing

* Contributions are made for a single issue, on that issue's branch.
* Contact the assignee of an issue before working on that issue.
* Code will follow the [Google Style Guide](https://google.github.io/styleguide/javaguide.html) with the following addenda:
	* Omission of *"4.2 Block indentation: +2 spaces"* and *"4.5.2 Indent continuation lines at least +4 spaces"*. Only tabs ([U+0009](https://en.wikipedia.org/wiki/Tab_key)) will be used for indentation.